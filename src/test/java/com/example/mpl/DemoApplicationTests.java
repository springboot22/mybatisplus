package com.example.mpl;

import com.baomidou.mybatisplus.annotation.TableId;
import com.example.mpl.entity.Product;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.logging.Logger;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(BlockJUnit4ClassRunner.class)
class DemoApplicationTests {

    @Test
    void contextLoads() throws IllegalAccessException {
        Product product = new Product();
        product.setId(14);
        Field[] fields1 = product.getClass().getDeclaredFields();
        for (Field field : fields1) {
            field.setAccessible(true);
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().getTypeName().equals(TableId.class.getTypeName())){
                    System.out.println("是这个");
                }
            }
            System.out.println(field.get(product));
        }
    }

}
