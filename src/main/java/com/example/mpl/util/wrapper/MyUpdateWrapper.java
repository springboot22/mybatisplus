package com.example.mpl.util.wrapper;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

/**
 * @author will.tuo
 * @date 2021/12/15 17:11
 */
public class MyUpdateWrapper<T> extends UpdateWrapper<T> {
    private final Class<T> clazz;
    public MyUpdateWrapper(Class<T> clazz){
        this.clazz = clazz;
    }
    public Class<T> getClazz(){
        return this.clazz;
    }
}
