package com.example.mpl.util.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2021/12/16 10:28
 * 用户可自定义Page的DTO 或者VO
 */
@Data
public class TtPageVo<T> {
    private int total;
    private int pageIndex;
    private int pageSize;
    private int resultCount;
    private List<T> results;

    public TtPageVo(IPage<T> page){
        this.pageIndex = (int)page.getCurrent();
        this.pageSize = (int)page.getSize();
        this.total = (int)page.getTotal();
        this.results = page.getRecords();
        this.resultCount = results.size();
    }
}
