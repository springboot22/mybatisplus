package com.example.mpl.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mpl.entity.Product;
import com.example.mpl.mapper.ProductMapper;
import com.example.mpl.service.ProductService;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/4 14:27
 */
@Service
public class ProcuctServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {
}
