package com.example.mpl.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mpl.entity.AppInfo;

import com.example.mpl.mapper.AppInfoMapper;

import com.example.mpl.service.AppInfoService;

import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/4 14:27
 */
@Service
public class AppInfoServiceImpl extends ServiceImpl<AppInfoMapper, AppInfo> implements AppInfoService {
}
