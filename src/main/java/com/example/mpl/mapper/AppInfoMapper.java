package com.example.mpl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mpl.entity.AppInfo;
import com.example.mpl.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author will.tuo
 * @date 2021/12/14 15:19
 */
@Mapper
@Repository
public interface AppInfoMapper extends BaseMapper<AppInfo> {

}
